#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stddef.h>

#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>

#include <pthread.h>

#include <signal.h>
#include <setjmp.h>

#define DEFAULT_SEGMENT_SIZE	(1024L*1024*1024)
#define DEFAULT_BLOCK_SIZE		8192
#define DEFAULT_TOTAL_SIZE		(100L*1024*1024*1024)

#define ERROR_SHMOPEN	(-1)
#define ERROR_FTRUNCATE	(-2)
#define	ERROR_MMAP		(-3)
#define ERROR_MUNMAP	(-4)
#define ERROR_SHUNLINK	(-5)
#define ERROR_ARGUMENTS	(-6)
#define ERROR_BARRIER	(-7)
#define ERROR_PTHREAD	(-8)

/* total segment size and block size (in bytes) */
long segment_size = DEFAULT_SEGMENT_SIZE;
long block_size = DEFAULT_BLOCK_SIZE;
long total_size = DEFAULT_TOTAL_SIZE;

/* shared segment */
char *segment = NULL;

/* benchmarking options */
int random_access = 0;   /* sequential access by default */
int num_of_threads = 1;  /* single-threaded by default */
int write_benchmark = 0; /* read-only by default */

/* output options */
int print_help = 0;
int print_csv = 0;
int verbose = 0;

/* interrupt flag */
volatile int terminate = 0;

/* barrier for syncing the threads */
pthread_barrier_t sync_barrier;
static sigjmp_buf sigbus_env;

static void terminate_handler(int signum);
static void do_init(int * fd);
static void do_cleanup();

/* options recognized on the command-line */
static struct option long_options[] = {
	{"size",    required_argument, 0,  's' },
	{"total",   required_argument, 0,  'm' },
	{"block",   required_argument, 0,  'b' },
	{"threads", required_argument, 0,  't' },
	{"random",  no_argument,       0,  'r' },
	{"csv",     no_argument,       0,  'c' },
	{"write",   no_argument,       0,  'w' },
	{"help",    no_argument,       0,  'h' },
	{"verbose", no_argument,       0,  'v' },
	{0,         0,                 0,   0  }
};

/* per-thread info - ID and how long it took to run the benchmark */
typedef struct thread_info {

	int     id;       /* thread ID */
	double  duration; /* benchmarking thread duration (in seconds) */

} thread_info;

/*
 * Runs the actual benchmark - either in a single process, or in multiple threads.
 * 
 * Meaning of most parameters should be clear, the only exception might be 'step'.
 * This is the distance between accessed blocks - with step=1, the thread will
 * access blocks 0, 1, 2, 3, ... while with step=3 the thread will access blocks
 * 0, 3, 6, 9, ... and so on. With large step values this is used to simulate
 * random access (the thread ID is factored in, so that different threads use
 * different step values).
 * 
 * In the future this might be improved to use a sufficiently good PRNG, as for
 * example mt_rand() or a similar one.
 * 
 * Also, this does not care about 'synchronization' effects yet - all the threads
 * start at block 0. With randomized access this is not a big deal, but with
 * sequential access this might result in caching.
 * 
 * Parameters:
 * 	- segment:      name of the shared segment
 * 	- segment_size: size of the shared segment in bytes
 * 	- total_size:   amount of memory to read (in this thread) in bytes
 * 	- block_size:   size of the blocks we should use when accessing the segment
 * 	- step:         distance between blocks to access (1 => sequential)
 * 
 * TODO Consired using a proper PRNG instead of the 'step' parameter.
 * TODO Consider randomizing the starting position (for each thread separately).
 * 
 */
void run_benchmark(char * segment, long segment_size, long total_size, int block_size, int step) {

	/* number of blocks to read */
	int nblocks = (total_size / block_size);

	/* last block in the segment */
	int maxblock = (segment_size / block_size);

	/* used to read memory */
	long i = 0, j = 0;
	char block[block_size];
	volatile char x = 0;

	if (write_benchmark) {

		/* init the block to random data */
		for (i = 0; i < block_size; i++) {
			block[i] = random() % 256;
		}

		/* write the block into the segment sequentially (step=1) or randomly (step >> 1) */
		for (i = 0; i < nblocks; i++)
		{
			/* remember this point for SIGBUS handling */
			sigsetjmp(sigbus_env, 1);

			if (terminate) { break; } /* interrupted */
			memcpy(&(segment[j*block_size]), block, block_size);
			j = (j + step) % maxblock;
		}

	} else {

		/* read the shared segment sequentially (step=1) or randomly (step >> 1) */
		for (i = 0; i < nblocks; i++)
		{
			/* remember this point for SIGBUS handling */
			sigsetjmp(sigbus_env, 1);

			if (terminate) { break; } /* interrupted */
			memcpy(block, &(segment[j*block_size]), block_size);
			j = (j + step) % maxblock;

			/* we need to do this to prevent compilers from optimizing out the memcpy */
			x += block[i % block_size];
		}
	}

	segment[0] = x;

}

/*
 * Wraps the benchmark so that it can be run within a thread. This is used only
 * if the '--threads' option is set to a value > 0, otherwise the benchmark is
 * executed directly.
 * 
 * The function synchronizes all threads at the start (on a barrier), executes
 * the benchmark and then measures the time spent in the benchmark.
 * 
 * This assumes that all the threads run with the same priority, i.e. with the
 * same priority when accessing CPU, memory etc. If this is not true, some of
 * the threads may take much longer to run. This is quite simple to detect
 * when the --verbose option - watch for significant differences between the
 * 'min duration' and 'max duration' lines.
 * 
 * This expects a single parameter - a pointer to thread_info structure. The
 * function expects 'id' to be set (unique value for each thread), and the
 * duration will be set to the 'duration' field (in seconds).
 * 
 */
void * run_benchmark_thread(void * info) {

	/* timing (start/stop) */
	struct timeval start_time, end_time;
	int sec = 0, usec = 0;

	/* wait on the barrier */
	int rc = pthread_barrier_wait(&sync_barrier);

	if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD) {
		printf("ERROR: failed to wait on the barrier\n");
		exit(ERROR_BARRIER);
	}

	/* run benchmark */
	gettimeofday(&start_time, NULL);
	run_benchmark(segment, segment_size, total_size/num_of_threads, block_size,
				  (random) ? (4447 + 10*((thread_info*)info)->id) : 1);
	gettimeofday(&end_time, NULL);

	/* compute duration */
	sec = end_time.tv_sec - start_time.tv_sec;
	usec = end_time.tv_usec - start_time.tv_usec;

	/* store the duration in the output structure */
	((thread_info*)info)->duration = sec + usec/1000000.0;

	/* pthread cleanup */
	pthread_exit(NULL);

}

/*
 * Simple parsing and validation of command-line options.
 */
int parse_arguments(int argc, char ** argv) {

	int c = 0;

	while (1) {
		int option_index = 0;
		c = getopt_long(argc, argv, "s:m:b:t:rwhvc", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
			case 'b':
				block_size = (long)atoi(optarg);
				break;
			case 'c':
				print_csv = 1;
				break;
			case 'h':
				print_help = 1;
				break;
			case 'v':
				verbose = 1;
				break;
			case 'm':
				total_size = (long)atoi(optarg) * 1024 * 1024 * 1024;
				break;
			case 'r':
				random_access = 1;
				break;
			case 's':
				segment_size = (long)atoi(optarg) * 1024 * 1024;
				break;
			case 't':
				num_of_threads = atoi(optarg);
				break;
			case 'w':
				write_benchmark = 1;
				break;
			default:
				/* invalid option - terminate the execution */
				exit(ERROR_ARGUMENTS);
		}
	}

	/* additional checks */

	/* segment size must not be negative */
	if (segment_size <= 0) {
		printf("ERROR: negative segment size\n");
		return -1;
	}

	/* check that segment is not larger than 2GB on 32-bit architectures */
	if ((sizeof(int*) == 4) && (segment_size > 2e32)) {
		printf("ERROR; segment too large (>2GB on 32-bit architecture\n");
		return -1;
	}

	/* total memory size must not be negative */
	if (total_size <= 0) {
		printf("ERROR: negative amount of memory\n");
		return -1;
	}

	/* block size must not be negative */
	if (block_size <= 0) {
		printf("ERROR: negative block size\n");
		return -2;
	}

	/* block size lower than segment size */
	if (block_size > segment_size) {
		printf("ERROR: block size greater than segment size\n");
		return -3;
	}

	/* number of threads not negative */
	if (num_of_threads < 1) {
		printf("ERROR: negative number of threads\n");
		return -4;
	}

	return 0;

}

/* prints help */
void print_help_info() {
	printf("POSIX shared memory benchmark:\n\n");
	printf(" -s, --size N      shared segment size in MB (default 1024 MB)\n");
	printf(" -t, --total N     amount of memory to read/write in GB (default 100 GB)\n");
	printf(" -b, --block N     block size in bytes (default 8 kB)\n");
	printf(" -t, --threads N   number of threads (default 1)\n");
	printf(" -r, --random      benchmark random access (default: sequential)\n");
	printf(" -w, --write       perform write benchmark (default: read-only)\n");
	printf(" -c. --csv         print CSV results on stderr (default: no)\n");
	printf(" -v, --verbose     verbose output (details about threads)\n");
	printf(" -h, --help        print this info\n\n");
}

/*
 * Performs initialization - sets signal handlers, allocates the  shared segment and
 * mmaps it into memory (so that the threads may access it).
 */
static
void do_init(int * fd) {

	/* install signal handler(s) */
	signal(SIGABRT, terminate_handler);
	signal(SIGINT,  terminate_handler);
	signal(SIGTERM, terminate_handler);
	signal(SIGBUS,  terminate_handler);

	/* Create shared memory object and set its size */
	*fd = shm_open("/shmbench", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	if (*fd == -1)
	{
		printf("ERROR: shm_open failed");
		exit(ERROR_SHMOPEN);
	}

	if (ftruncate(*fd, segment_size) == -1)
	{
		printf("ERROR: ftruncate failed");
		do_cleanup();
		exit(ERROR_FTRUNCATE);
	}

	/* Map shared memory object */
	segment = mmap(NULL, segment_size, PROT_READ | PROT_WRITE, MAP_SHARED, *fd, 0);
	if (segment == MAP_FAILED) {
		printf("ERROR: mmap failed");
		do_cleanup();
		exit(ERROR_MMAP);
	}

}

/*
 * Does about the opposite to the init - detaches the segment, performs pthread
 * cleanup (if threads were used).
 */
static
void do_cleanup() {

	/* cleanup */
	if ((segment != NULL) && (segment != MAP_FAILED)) {
		if (munmap(segment, segment_size) != 0)
		{
			printf("ERROR: munmap failed");
			exit(ERROR_MUNMAP);
		}
	}

	if (shm_unlink("/shmbench") != 0)
	{
		printf("ERROR: shm_unlink failed");
		exit(ERROR_SHUNLINK);
	}

	if (num_of_threads > 1) {
		pthread_exit(NULL);
	}

}

/*
 * Main method.
 * 
 * 1) parses the command-line options, prints help if requested
 * 
 * 2) performs initialization (creates/mmaps segment, ...)
 * 
 * 3) executes the benchmark - either directly (single process), or using threads
 * 
 * 4) collects and prints results
 * 
 */
int main(int argc, char ** argv) {

	int fd;

	/* timing (start/stop) */
	struct timeval start_time, end_time;
	int sec = 0, usec = 0;

	if (parse_arguments(argc, argv) != 0)
	{
		printf("ERROR: invalid command-line arguments\n");
		return ERROR_ARGUMENTS;
	}

	if (print_help)
	{
		print_help_info();
		return 0; /* everything went fine */
	}

	do_init(&fd);

	/* print benchmark info */
	printf("block size = %ld\n", block_size);
	printf("total size = %ld\n", segment_size);
	printf("threads = %d\n", num_of_threads);
	printf("random = %s\n", (random_access == 0) ? "NO" : "YES");
	printf("write = %s\n", (write_benchmark == 0) ? "NO" : "YES");

	gettimeofday(&start_time, NULL);

	if (num_of_threads == 1) {
		/* single-threaded - no point in starting the pthread machinery */
		run_benchmark(segment, segment_size, total_size, block_size, (random) ? 4447 : 1);
	} else {
		/* multi-threaded benchmark */
		long i = 0;
		pthread_t threads[num_of_threads];
		thread_info info[num_of_threads];

		/* initialize the barrier */
		if (pthread_barrier_init(&sync_barrier, NULL, num_of_threads)) {
			printf("ERROR: initialization of the barrier failed\n");
			exit(ERROR_BARRIER);
		}

		for (i = 0; i < num_of_threads; i++) {

			info[i].id = i;
			info[i].duration = 0;

			int rc = pthread_create(&threads[i], NULL, run_benchmark_thread, &info[i]);
			if (rc){
				printf("ERROR; return code from pthread_create() is %d\n", rc);
				exit(ERROR_PTHREAD);
			}
		}

		for (i = 0; i < num_of_threads; i++) {
			void * retval;
			pthread_join(threads[i], &retval);
		}

		/* print verbose info with details about threads */
		if (verbose) {

			double min_duration = info[0].duration,
				   max_duration = info[0].duration;

			printf("========= threads info ========\n");
			for (i = 0; i < num_of_threads; i++) {
				printf(" thread %02d : duration=%f\n", info[i].id, info[i].duration);

				/* get min/max duration */
				min_duration = (min_duration > info[i].duration) ? info[i].duration : min_duration;
				max_duration = (max_duration < info[i].duration) ? info[i].duration : max_duration;
			}
			printf("===============================\n");
			printf(" min duration = %f\n", min_duration);
			printf(" max duration = %f\n", max_duration);
			printf(" difference   = %.2f%%\n", 100*(max_duration - min_duration)/min_duration);
			printf("===============================\n");

		}

	}

	gettimeofday(&end_time, NULL);

	/* compute duration */
	sec = end_time.tv_sec - start_time.tv_sec;
	usec = end_time.tv_usec - start_time.tv_usec;

	/* print results only if not terminated by a signal */
	if (terminate) {
		printf("terminated by signal handler :-(\n");
	} else {

		printf("scanned = %ld MB\n", (total_size / 1024 / 1024));
		printf("duration = %.3f sec\n", (sec + (float)usec/1000000));
		printf("bandwidth = %.0f MB/s\n", (total_size / 1024 / 1024) / (sec + (float)usec/1000000));

		/* print csv */
		if (print_csv)
		{
			fprintf(stderr, "%ld\t%ld\t%d\t%ld\t%.3f\t%.3f\n", segment_size/1024/1024, block_size,
					num_of_threads, total_size/1024/1024,
					(sec + (float)usec/1000000),
					(total_size / 1024 / 1024) / (sec + (float)usec/1000000));
		}

	}

	do_cleanup();

	return 0;

}

static
void terminate_handler(int signum) {

	/* set the volatile value, so threads know to terminate */
	terminate = 1;

	/* in case of SIGBUS jump to the saved state */
	if (signum == SIGBUS) {
		siglongjmp (sigbus_env, 1);
	}

}