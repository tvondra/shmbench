shmbench (shared memory benchmark)
==================================

This is a very simple benchmark for testing memory bandwidth on shared memory
segments. This benchmark is using POSIX shared memory API, i.e. the memory segments
are attached using shm_open/mmap but that's an implementation detail - AFAIK
there's no significant performance difference between this and other implementations
(as for example SysV shared memory).

The benchmark is (intentionally) very simple and is not meant to be an ultimate
memory benchmark. If you're looking for more feature-rich tool, you may check
sysbench [https://launchpad.net/sysbench] for example. I've had some problems with
it (see for example [bug #1052896](https://bugs.launchpad.net/sysbench/+bug/1052896)
describing bogus results due to a compiler removing parts or the code).

Also, there are other simple benchmarks testing other aspects - a good example of
such benchmark is [stream](http://www.cs.virginia.edu/stream/).


Compilation
-----------
There's a very simple Makefile, so all you have to do to compile it is run "make".
That should give you 'shmbench' binary in the current directory.


Usage
-----
The command has a '--help' option just like any other well-behaved tool. Using it will
give you this:

    $ ./shmbench --help
    POSIX shared memory benchmark:

    -s, --size N      shared segment size in MB (default 1024 MB)
    -m, --total N     amount of memory to read/write in GB (default 100 GB)
    -b, --block N     block size in bytes (default 8 kB)
    -t, --threads N   number of threads (default 1)
    -r, --random      benchmark random access (default: sequential)
    -w, --write       perform write benchmark (default: read-only)
    -c, --csv         print CSV results on stderr (default: no)
    -v, --verbose     verbose output (details about threads)
    -h, --help        print this info
    
The '--size' determines size of the shared segment the benchmark should allocate in MBs.
The amount of memory to actually read/write, use '--total' (in GBs) and the size of the
blocks read/written is specified using '--block' (in bytes). So to read 50GB of data
using a 2GB shared segment in 8kB blocks, do this:

    $ ./shmbench --size 2048 --total 50 --block 8192
    scanned = 51200 MB
    duration = 4.939 sec
    bandwidth = 10365 MB/s

This is a read-only single-threaded benchmark by default, so if you want a write bench,
use '--write' switch. And if you want multi-threaded benchmark, use '--threads 4' (or
other number of threads).

The benchmark is also sequential by default - to get a random benchmark, use '--random'.

To get results in a form that's easy to process, use '--csv' switch - with it the results
are printed onto stderr in a CSV format (separated by tabs). The column order is

    # shared segment size (MBs)
    # block size (bytes)
    # number of threads
    # amount of memory (GBs)
    # duration
    # bandwidth (MB/s)