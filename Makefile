CFLAGS=-O3 -lrt -lpthread

shmbench: shmbench.c
	$(CC) -o shmbench shmbench.c $(CFLAGS)

clean:
	rm shmbench